## Description 

Pour ce projet quiz, j'ai choisi de faire quelque chose autour de l'univers de la famille royale britannique. 
Comme fonctionnalités, il comporte un compteur de points, un écran de début et un écran de fin.
Il est visible à l'adresse suivante: https://quiz-god-save-the-queen.netlify.app

## Visuals
![Maquette de l'écran de début](maquette-quiz.jpg)
![Maquette de l'écran de quiz](maquette-quiz-1.jpg)
![Maquette de l'écran de fin](maquette-quiz-2.jpg)
