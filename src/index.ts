//Variables
const divQuestion = document.querySelector<HTMLDivElement>("#question")
const op1 = document.querySelector<HTMLButtonElement>("#op1")
const op2 = document.querySelector<HTMLButtonElement>("#op2")
const op3 = document.querySelector<HTMLButtonElement>("#op3")
const divOptions = document.querySelector<HTMLDivElement>("#options")
const main = document.querySelector<HTMLDivElement>("#main")
const endscreen = document.querySelector<HTMLDivElement>("#endscreen")
const startscreen = document.querySelector<HTMLDivElement>("#startscreen")
const startButton = document.querySelector<HTMLButtonElement>("#start")
const restartButton = document.querySelector<HTMLButtonElement>("#restart")

//Index tableau de questions + réponses
let index = 0

//Index double tableau réponses
let k = 0
let l = 1
let m = 2

//Affichage du score
let results = document.querySelector<HTMLDivElement>("#results")
let finalResults = document.querySelector<HTMLDivElement>("#finalResults")
let score = 0

//Variable pour le choix actuel
let currentChoice = ""

// Tableaux questions et réponses
let questions = ["What famous punk band made fun of the british national anthem with a song named 'God Save The Queen' in 1977?", "How long did Queen Elizabeth II’s reign lasted?", "Who made Princess Diana’s famous 'revenge dress'?", "Which King Charles’ fact among these is true?", "'Dorgis' are:", "What is Queen Elizabeth II's favourite song?", "What was the profession of Antony Armstrong-Jones, Princess Margaret's ex-husband?", "What is King Charles' favourite show?", "What will you never see at Dracula's and the Windsor's dining table?", "Which rockstar was NOT knighted by Queen Elizabeth?"]
let answers = [
  ["A. Ed Sheeran", "B. The Sex Pistols", "C. The Beatles"],
  ["A. Since the dinosaurs extincted", "B. 2022 years (she’s basically Christ then)", "C. 70 years, 2 months & 4 days"],
  ["A. Christina Stambolian", "B. Vivienne Westwood", "C. Karl Lagerfeld"],
  ["A. He has a Blink-182 tattoo", "B. He was the cutest royal baby ever!", "C. He doesn’t squeeze his own toothpaste on his toothbrush"],
  ["A. creatures of Hell sent to exterminate the human race", "B. the Queen’s own breed of dogs", "C. Prince William’s nickname to his wife Kate Middleton"], 
  ["A. 'Rock & Roll Queen' by The Subways", "B. any song by the band Queen", "C.'Dancing Queen' by ABBA"],
  ["A. A photographer", "B. An actor", "C. A gardener"],
  ["A. Emmerdale", "B. The Crown", "C. Doctor Who"],
  ["A. Glasses of blood", "B. Onions", "C. Garlic"],
  ["A. Paul McCartney", "B. Johnny Rotten", "C. Mick Jagger"]
];
let isCorrect = ["B. The Sex Pistols", "C. 70 years, 2 months & 4 days", "A. Christina Stambolian", "C. He doesn’t squeeze his own toothpaste on his toothbrush", "B. the Queen’s own breed of dogs", "C.'Dancing Queen' by ABBA", "A. A photographer", "A. Emmerdale", "C. Garlic", "B. Johnny Rotten"]

//Fonction d'affichage du score
function updateScore() {
  if (results) {
    results.textContent = String(score);
  }
}

//Fonction du bouton start
if(startButton){
  startButton?.addEventListener('click', ()=>{
    console.log("Commencer");
    startQuiz()
  })
}

function startQuiz(){
  if (main !== null){
    main.style.display="flex";
    }
    if (startscreen !== null){
    startscreen.style.display="none";
    }
  score = 0;
  index = 0;
  change()
  updateScore()
}

//Fonction du bouton restart
function resetQuiz(){
  if (main !== null){
    main.style.display="flex";
  }
  if (endscreen !== null){
    endscreen.style.display="none";
  }
  score = 0;
  index = 0;
  change()
  updateScore()
}

if(restartButton){
  restartButton?.addEventListener('click', ()=>{
    console.log("Recommencer");
    resetQuiz()
  })
}

//Typage et affichage des questions
  if (divQuestion !== null)
  {divQuestion.innerHTML = questions[index]}

  if (op1 !== null)
  {op1.innerHTML = answers[index][k]}

  if (op2 !== null)
  {op2.innerHTML = answers[index][l]}

  if (op3 !== null)
  {op3.innerHTML = answers[index][m]}


//Changement de lot questions/réponses au clic
if(divOptions){
  op1?.addEventListener('click', ()=>{
    currentChoice = answers[index][k];
    compare();
    index++;
    endQuiz();
    change();
  })
  op2?.addEventListener('click', ()=>{
    currentChoice = answers[index][l];
    compare();
    index++;
    endQuiz();
    change();
  })
  op3?.addEventListener('click', ()=>{
    currentChoice = answers[index][m];
    compare();
    index++;
    endQuiz();
    change();
  })
}

//Fonction pour changer de question & de lot de réponses
function change(){
  if (index < questions.length){

  } else return 

  if (divQuestion && op1 && op2 && op3){
      divQuestion.innerHTML = questions[index]
      op1.innerHTML = answers[index][k]
      op2.innerHTML = answers[index][l]
      op3.innerHTML = answers[index][m]
    } 

}

//Fonction pour comparer answers & isCorrect et incrémenter le score
function compare(){
  if (currentChoice == isCorrect[index]) {
    score++
    updateScore()
    console.log("vrai")
    console.log(isCorrect[index])
  }if (currentChoice != isCorrect[index]){
    console.log("faux")
    console.log(isCorrect[index])
    console.log(currentChoice);
  }
}

// Fonction pour finir le quiz
function endQuiz(){
  if (index >= questions.length){
    updateScore();
    console.log("End of the game");

    if (finalResults) {
      finalResults.textContent = String(score);
    }

    if (main !== null){
    main.style.display="none";
    }
    if (endscreen !== null){
    endscreen.style.display="flex";
    }
  }
}